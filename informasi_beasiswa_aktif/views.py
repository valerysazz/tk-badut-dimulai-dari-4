from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse

def index(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']
		return render(request, 'informasi_beasiswa_aktif.html', response)
	else:
		return HttpResponseRedirect(reverse('account:login'))