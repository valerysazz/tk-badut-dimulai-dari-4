from django.apps import AppConfig


class InformasiBeasiswaAktifConfig(AppConfig):
    name = 'informasi_beasiswa_aktif'
