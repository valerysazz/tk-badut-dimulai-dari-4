from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^submit-tempat-wawancara', submit_tempat_wawancara, name='submit-donatur-yayasan'),
]
