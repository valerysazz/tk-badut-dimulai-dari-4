from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

def index(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']
		if (response['role'] == 'admin'):
			return render(request, 'wawancara.html', response)
	else:
		return HttpResponseRedirect(reverse('account:login'))

def submit_tempat_wawancara(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']

		if (response['role'] == 'admin'):
			kode = request.POST['kodetempatwawancara']
			nama = request.POST['namatempatwawancara']
			lokasi = request.POST['lokasitempatwawancara']

			cursor = connection.cursor()
			cursor.execute("SELECT * from tempat_wawancara where kode='"+kode+"'")
			result = cursor.fetchone();
			if (result):
				if len(result) > 0:
					response['error'] = "Kode"
					response['isi'] = kode
					return render(request, 'wawancara_fail.html', response)

			values = "'"+kode+"','"+nama+"','"+lokasi+"'"
			query = "INSERT INTO tempat_wawancara (kode,nama,lokasi) values (" + values + ")"
			cursor.execute(query)

			return render(request, 'wawancara_success.html', response)

	else:
		return HttpResponseRedirect(reverse('account:login'))