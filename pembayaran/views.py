from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

def index(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']
		if (response['role'] == 'yayasan' or response['role'] == 'individual_donor'):
			return render(request, 'pembayaran.html', response)
	else:
		return HttpResponseRedirect(reverse('account:login'))

def submit_pembayaran(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']

		if (response['role'] == 'yayasan' or response['role'] == 'individual_donor'):
			urutan = request.POST['pembayaranurutan']
			kode = request.POST['pembayarankodeskemabeasiswa']
			no_urut = request.POST['pembayarannourutskemabeasiswa']
			npm = request.POST['pembayarannpm']
			keterangan = request.POST['pembayaranketerangan']
			tanggal = request.POST['pembayarantanggalbayar']
			nominal = request.POST['pembayarannominal']

			cursor = connection.cursor()
			cursor.execute("SELECT * FROM skema_beasiswa_aktif WHERE no_urut = '"+no_urut+"' AND kode_skema_beasiswa = '"+kode+"'")
			result = cursor.fetchone();
			if (result):
				if len(result) > 0:
					cursor.execute("SELECT * FROM pendaftaran WHERE no_urut = '"+no_urut+"' AND kode_skema_beasiswa = '"+kode+"' AND npm = '"+npm+"' AND status_terima = 'Aktif'")
					result = cursor.fetchone();
					if (result):
						if len(result) > 0:
							values = "'"+urutan+"','"+kode+"','"+no_urut+"','"+npm+"','"+keterangan+"','"+tanggal+"','"+nominal+"'"
							query = "INSERT INTO pembayaran (urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) values (" + values + ")"
							cursor.execute(query)
							return render(request, 'pembayaran_success.html', response)
					else:
						response['error'] = "NPM"
						response['isi'] = npm
						return render(request, 'pembayaran_fail.html', response)
			else:
					response['error'] = "No Urut Skema Beasiswa"
					response['isi'] = no_urut
					return render(request, 'pembayaran_fail.html', response)

	else:
		return HttpResponseRedirect(reverse('account:login'))

							