from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^submit-pembayaran', submit_pembayaran, name='submit-pembayaran'),
]
