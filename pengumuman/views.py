from django.shortcuts import render
from django.db import models
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse

def index(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']

		cursor = connection.cursor()
		cursor.execute("SELECT * from pengumuman")
		result = cursor.fetchall();
		response['data'] = convert_to_dict(result)
		return render(request, 'pengumuman.html', response)
	else:
		return HttpResponseRedirect(reverse('account:login'))

def convert_to_dict(queryset):
	ret_val = []
	for data in queryset:
		dict = {}
		dict['tanggal'] = data[0]
		dict['no_urut'] = data[1]
		dict['kode'] = data[2]
		dict['username'] = data[3]
		dict['judul'] = data[4]
		dict['isi'] = data[5]
		ret_val.append(dict)
	return ret_val

def buat_pengumuman(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']

		if (response['role'] == 'yayasan' or response['role'] == 'individual_donor' or response['role'] == 'admin'):
			return render(request, 'buat_pengumuman.html', response)

	else:
		return HttpResponseRedirect(reverse('account:login'))


def submit_pengumuman(request):
	response = {}
	if ('username' in request.session.keys()):
		response['role'] = request.session['role']
		if ('nama' in request.session.keys()):
			response['nama'] = request.session['nama']
		response['username'] = request.session['username']

		if (response['role'] == 'yayasan' or response['role'] == 'individual_donor' or response['role'] == 'admin'):
			tgl = request.POST.get(models.DateTimeField(auto_now_add=True))
			tanggal = ""
			no_urut = request.POST.get('pengumumannourut', False)
			kode = request.POST.get('pengumumankodeskema', False)
			judul = request.POST.get('pengumumanjudul', False)
			isi = request.POST.get('pengumumanisi', False)

			cursor = connection.cursor()

			value = "'"+tanggal+"','"+no_urut+"','"+kode+"',username,'"+judul+"','"+isi+"'"
			values = str(value)
			query = "INSERT INTO pengumuman (tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username, judul, isi) values (" + values + ")"
			cursor.execute(query)

			return render(request, 'pengumuman_success.html', response)

	else:
		return HttpResponseRedirect(reverse('account:login'))