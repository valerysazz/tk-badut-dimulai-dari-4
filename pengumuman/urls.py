from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^buat-pengumuman', buat_pengumuman, name='buat_pengumuman'),
    url(r'^submit-pengumuman', submit_pengumuman, name='submit_pengumuman'),
]
