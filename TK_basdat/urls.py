"""TK_basdat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import login.urls as account
import wawancara.urls as wawancara
import pengumuman.urls as pengumuman
import pembayaran.urls as pembayaran
import pendaftaran_skema.urls as pendaftaran_skema
import informasi_beasiswa_aktif.urls as informasi_beasiswa_aktif
from login.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^account/', include(account, namespace='account')),
    url(r'^wawancara/', include(wawancara, namespace='wawancara')),
    url(r'^pengumuman/', include(pengumuman, namespace='pengumuman')),
    url(r'^pembayaran/', include(pembayaran, namespace='pembayaran')),
    url(r'^pendaftaran_skema/', include(pendaftaran_skema, namespace='pendaftaran_skema')),
    url(r'^informasi_beasiswa_aktif/', include(informasi_beasiswa_aktif, namespace='informasi_beasiswa_aktif')),
    url(r'^$', home, name='home'),
    
]
