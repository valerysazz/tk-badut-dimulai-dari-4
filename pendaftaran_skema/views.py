from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_protect
from django.db import connection
from django.contrib import messages

def index(request):
    response = {}
    if ('username' in request.session.keys()):
        response['role'] = request.session['role']
        if ('nama' in request.session.keys()):
            response['nama'] = request.session['nama']
        response['username'] = request.session['username']

        if (response['role'] == 'yayasan' or response['role'] == 'individual_donor'):
            return render(request, 'pendaftaran_skema.html')

    else:
        return HttpResponseRedirect(reverse('account:login'))

def sukses(request):
    response = {}
    if ('username' in request.session.keys()):
        response['role'] = request.session['role']
        if ('nama' in request.session.keys()):
            response['nama'] = request.session['nama']
        response['username'] = request.session['username']
        return render(request, 'sukses.html')
    else:
        return HttpResponseRedirect(reverse('account:login'))


@csrf_protect
@require_http_methods(["POST"])
def daftar_paket(request):
    response = {}
    if ('username' in request.session.keys()):
        response['role'] = request.session['role']
        if ('nama' in request.session.keys()):
            response['nama'] = request.session['nama']
        response['username'] = request.session['username']

        if (response['role'] == 'yayasan' or response['role'] == 'individual_donor'):
            username=request.session['username']
            
            print(request.POST)

            kode = request.POST.get('kode')
            nama= request.POST.get('nama')
            jenis= request.POST.get('jenis')
            deskripsi = request.POST.get('deskripsi')
            syarat = request.POST.get('syarat')
            print(kode)
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM skema_beasiswa WHERE kode=%s",(kode,))
            result = cursor.fetchone()
            if(result):
                if len(result)>0:
                    messages.error(request, "Pendaftaran Gagal: Kode " +kode+" sudah ada")
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

            

            cursor = connection.cursor()
            cursor.execute("SELECT nomor_identitas from donatur where username='"+username+"'")
            result = cursor.fetchone()
            no_id = result[0]

            

            query = "INSERT INTO skema_beasiswa(kode,nama,jenis,deskripsi,no_identitas_donatur) values (%s, %s, %s, %s, %s)"
            data = (kode, nama, jenis, deskripsi, no_id)
            cursor.execute(query, data)

            cursor = connection.cursor()
            query = "INSERT INTO syarat_beasiswa (kode_beasiswa,syarat) values ('"+kode+"','"+syarat+"')"
            cursor.execute(query)

            messages.success(request, "Pendaftaran Berhasil")

            return HttpResponseRedirect(reverse('pendaftaran_skema:sukses'))
    else:
        return HttpResponseRedirect(reverse('account:login'))