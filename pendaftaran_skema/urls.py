from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^daftar_paket_beasiswa/', daftar_paket, name='daftar_paket'),
    url(r'^pendaftaran-sukses/', sukses, name='sukses'),
    
]
